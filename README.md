# JpaRepositoryWithJoinedInheritance


This is a sample app which implement three classes. **ChildOne** and **ChildTwo** which inherit from **Parent**.

It also include the repositories (DAO). All of them inherits from **BaseRepository** which extends **JpaRepository**. The inheritance strategy is **JOINED**.

You can just run the test class after setting up your own database. 

This repo will be updated for including more details in the future. 


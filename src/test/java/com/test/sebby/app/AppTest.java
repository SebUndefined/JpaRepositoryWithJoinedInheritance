package com.test.sebby.app;

import java.util.List;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import com.test.sebby.dao.ChildOneRepository;
import com.test.sebby.entity.ChildOne;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AppTest extends TestCase
{
	@Autowired
	ChildOneRepository childOneRepository;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest()
    {
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    @org.junit.Test
    public void testApp()
    {
    	List<ChildOne> childOnes = childOneRepository.findAll();
    	for(int i = 0; i<childOnes.size(); i++) {
    		System.out.println(childOnes.get(i).toString());
    	}
    	ChildOne childOne = new ChildOne();
    	childOne.setName("Prout");
    	childOneRepository.save(childOne);
        assertTrue( true );
    }
    
}

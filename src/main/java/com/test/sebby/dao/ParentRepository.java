package com.test.sebby.dao;




import org.springframework.stereotype.Repository;

import com.test.sebby.entity.Parent;

@Repository
public interface ParentRepository extends BaseRepository<Parent> {

}

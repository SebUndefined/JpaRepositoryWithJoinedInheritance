package com.test.sebby.dao;

import org.springframework.stereotype.Repository;

import com.test.sebby.entity.ChildTwo;

@Repository
public interface ChildTwoRepository extends BaseRepository<ChildTwo> {

}

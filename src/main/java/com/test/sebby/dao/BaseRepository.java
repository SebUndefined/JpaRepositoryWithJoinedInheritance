package com.test.sebby.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.test.sebby.entity.Parent;

@NoRepositoryBean
public interface BaseRepository<T extends Parent> extends JpaRepository<T, Long> {

}

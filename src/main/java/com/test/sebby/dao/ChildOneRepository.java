package com.test.sebby.dao;


import org.springframework.stereotype.Repository;

import com.test.sebby.entity.ChildOne;


@Repository
public interface ChildOneRepository extends BaseRepository<ChildOne> {

}

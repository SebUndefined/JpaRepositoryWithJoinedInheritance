package com.test.sebby.entity;

import javax.persistence.Entity;

@Entity
public class ChildOne extends Parent {
	
	private String nameOne;

	public String getName() {
		return nameOne;
	}

	public void setName(String name) {
		this.nameOne = name;
	}

	@Override
	public String toString() {
		return "ChildOne [nameOne=" + nameOne + ", id=" + id + "]";
	}
	
	

}

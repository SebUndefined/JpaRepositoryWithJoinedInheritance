package com.test.sebby.entity;

import javax.persistence.Entity;


@Entity
public class ChildTwo extends Parent {
	
	private String nameTwo;

	public String getName() {
		return nameTwo;
	}

	public void setName(String name) {
		this.nameTwo = name;
	}

}
